# Java RPG Assignment Sample

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

Sample app using JDBC and Repositories to access a database

This is done to provide a simple solution to the requirements set forth.
It is simply one solution, not the only solution. This solution implements 
the requirements to be SOLID and DRY - although, some aspects are not SOLID 
by design to be a simple implementation.

All requirements are represented as issues with branches and merging to 
integrate. A CI pipeline is created to run tests on any commits to the 
`main` branch and merge requests.

A branch is in the works with a refactored version which includes some design 
patterns and a 
more SOLID approach.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```bash
git clone git-url
cd project-directory
./gradlew build
```

## Usage

```bash
./gradlew test
```

## Maintainers

[@NicholasLennox](https://gitlab.com/NicholasLennox)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Noroff Accelerate
