package no.accelerate.util.equipment;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
