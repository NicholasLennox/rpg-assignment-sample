package no.accelerate.util.attribute;

public class PrimaryAttribute {
    private int strength;
    private int dexterity;
    private int intelligence;

    // Constructor
    public PrimaryAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    //  ---------
    //  GENERATED
    //  ---------

    // Generated equals and getHashCode

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrimaryAttribute that = (PrimaryAttribute) o;

        if (strength != that.strength) return false;
        if (dexterity != that.dexterity) return false;
        return intelligence == that.intelligence;
    }

    @Override
    public int hashCode() {
        int result = strength;
        result = 31 * result + dexterity;
        result = 31 * result + intelligence;
        return result;
    }

    // Generated getters and setters

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    /**
     * Increases the current set of attributes.
     */
    public void increase(int strength, int dexterity, int intelligence) {
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
    }
}
