package no.accelerate.item.util;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
