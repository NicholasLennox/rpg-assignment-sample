package no.accelerate.item;

import no.accelerate.item.util.WeaponType;
import no.accelerate.util.equipment.Slot;

/**
 * Extension of item to represent a weapon. Weapons help characters deal damage.
 * All weapons are created in the weapon slot by default, as to avoid any logic errors.
 */
public class Weapon extends Item {
    private final double damage;
    private final double speed;
    private final WeaponType weaponType;

    public Weapon(String name, int requiredLevel, WeaponType weaponType, double damage, double speed) {
        super(name, Slot.WEAPON, requiredLevel);
        this.damage = damage;
        this.speed = speed;
        this.weaponType = weaponType;
    }

    public double getDps() {
        return damage*speed;
    }

    // ---------
    // GENERATED
    // ---------

    public WeaponType getWeaponType() {
        return weaponType;
    }
}
