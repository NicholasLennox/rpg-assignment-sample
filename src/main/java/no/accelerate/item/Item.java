package no.accelerate.item;

import no.accelerate.util.equipment.Slot;

/**
 * Base abstraction for all items. Common base to encapsulate how the item can be equipped.
 * In other implementations, this behaviour could be done differently.
 */
public abstract class Item {
    private final String name;
    private final Slot slot;
    private final int requiredLevel;

    public Item(String name, Slot slot, int requiredLevel) {
        this.name = name;
        this.slot = slot;
        this.requiredLevel = requiredLevel;
    }

    // ---------
    // GENERATED
    // ---------

    public String getName() {
        return name;
    }

    public Slot getSlot() {
        return slot;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }
}
