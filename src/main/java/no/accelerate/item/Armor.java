package no.accelerate.item;

import no.accelerate.item.util.ArmorType;
import no.accelerate.util.attribute.PrimaryAttribute;
import no.accelerate.util.equipment.Slot;

/**
 * Extension of item to represent armor. Armor help characters deal damage through bonus attributes.
 */
public class Armor extends Item {
    private final PrimaryAttribute bonusAttributes;
    private final ArmorType armorType;

    public Armor(String name, Slot slot, int requiredLevel, ArmorType armorType, int bonusStrength, int bonusDexterity, int bonusIntelligence) {
        super(name, slot, requiredLevel);
        this.armorType = armorType;
        bonusAttributes = new PrimaryAttribute(bonusStrength, bonusDexterity, bonusIntelligence);
    }

    // ---------
    // GENERATED
    // ---------

    public PrimaryAttribute getBonusAttributes() {
        return bonusAttributes;
    }

    public ArmorType getArmorType() {
        return armorType;
    }
}
