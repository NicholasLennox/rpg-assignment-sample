package no.accelerate.item.exception;

public class InvalidWeaponException extends Exception {
    public InvalidWeaponException(String message) {
        super(message);
    }
}
