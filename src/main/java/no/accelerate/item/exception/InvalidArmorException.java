package no.accelerate.item.exception;

public class InvalidArmorException extends Exception {
    public InvalidArmorException(String message) {
        super(message);
    }
}
