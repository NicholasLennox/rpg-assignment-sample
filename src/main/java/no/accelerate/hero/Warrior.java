package no.accelerate.hero;

import no.accelerate.item.util.ArmorType;
import no.accelerate.item.util.WeaponType;

import java.util.List;

/**
 * Extension of Hero to supply Warrior-specific behaviour.
 */
public class Warrior extends Hero {
    public Warrior(String name) {
        super(name, 5, 2, 1);
        validWeaponTypes = List.of(WeaponType.AXE, WeaponType.HAMMER, WeaponType.SWORD);
        validArmorTypes = List.of(ArmorType.MAIL, ArmorType.PLATE);
    }

    @Override
    public String levelUp() {
        characterAttributes.increase(3,2,1);
        return super.levelUp();
    }
}
