package no.accelerate.hero;

import no.accelerate.item.util.ArmorType;
import no.accelerate.item.util.WeaponType;

import java.util.List;

/**
 * Extension of Hero to supply Rogue-specific behaviour.
 */
public class Rogue extends Hero {
    public Rogue(String name) {
        super(name, 2, 6, 1);
        validWeaponTypes = List.of(WeaponType.DAGGER, WeaponType.SWORD);
        validArmorTypes = List.of(ArmorType.LEATHER, ArmorType.MAIL);
    }

    @Override
    public String levelUp() {
        characterAttributes.increase(1,4,1);
        return super.levelUp();
    }
}
