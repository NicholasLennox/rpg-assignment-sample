package no.accelerate.hero;

import no.accelerate.item.Armor;
import no.accelerate.item.Weapon;
import no.accelerate.item.exception.InvalidArmorException;
import no.accelerate.item.exception.InvalidWeaponException;
import no.accelerate.item.util.ArmorType;
import no.accelerate.item.util.WeaponType;

import java.util.Arrays;
import java.util.List;

/**
 * Extension of Hero to supply Mage-specific behaviour.
 */
public class Mage extends Hero {
    public Mage(String name) {
        super(name, 1, 1, 8);
        validWeaponTypes = List.of(WeaponType.STAFF, WeaponType.WAND);
        validArmorTypes = List.of(ArmorType.CLOTH);
    }

    @Override
    public String levelUp() {
        characterAttributes.increase(1,1,5);
        return super.levelUp();
    }
}
