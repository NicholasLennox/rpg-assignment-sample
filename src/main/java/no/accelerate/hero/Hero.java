package no.accelerate.hero;

import no.accelerate.item.Armor;
import no.accelerate.item.Item;
import no.accelerate.item.Weapon;
import no.accelerate.item.exception.InvalidArmorException;
import no.accelerate.item.exception.InvalidWeaponException;
import no.accelerate.item.util.ArmorType;
import no.accelerate.item.util.WeaponType;
import no.accelerate.util.attribute.PrimaryAttribute;
import no.accelerate.util.equipment.Slot;

import java.util.HashMap;
import java.util.List;

/**
 * Base abstraction for all heroes.
 * Has common functionality encapsulated to be DRY.
 * Can be improved ito SOLID, however, this project is a direct basic implementation
 *  with some SOLID included. A branch with a more separated implementation is under way.
 */
public abstract class Hero {
    private final String name;
    private int level = 1;
    protected PrimaryAttribute characterAttributes;
    private HashMap<Slot, Item> equipment;
    protected List<WeaponType> validWeaponTypes;
    protected List<ArmorType> validArmorTypes;

    // Defer starting attributes to child classes
    public Hero(String name, int strength, int dexterity, int intelligence) {
        this.name = name;
        characterAttributes = new PrimaryAttribute(strength,dexterity,intelligence);
        initializeEquipment();
    }

    // Start with all slots predefined and empty
    private void initializeEquipment() {
        equipment = new HashMap<>();
        equipment.put(Slot.HEAD, null);
        equipment.put(Slot.BODY, null);
        equipment.put(Slot.LEGS, null);
        equipment.put(Slot.WEAPON, null);
    }

    public String equip(Weapon weapon) throws InvalidWeaponException {
        // Level too high
        if(weapon.getRequiredLevel() > level)
            throw new InvalidWeaponException("Weapon level too high!");
        // Incorrect weapon type
        if(!validWeaponTypes.contains(weapon.getWeaponType()))
            throw new InvalidWeaponException("Wrong weapon type!");
        // Passed all guards
        equipment.put(weapon.getSlot(), weapon);
        return "Weapon equipped!";
    }

    public String equip(Armor armor) throws InvalidArmorException {
        // Level too high
        if(armor.getRequiredLevel() > level)
            throw new InvalidArmorException("Armor level too high!");
        // Incorrect armor type
        if(!validArmorTypes.contains(armor.getArmorType()))
            throw new InvalidArmorException("Wrong armor type!");
        // Passed all guards
        equipment.put(armor.getSlot(), armor);
        return "Armor equipped!";
    }

    /**
     * Increases the level of a Hero by 1 and increases its attributes.
     * @return Level up message
     */
    public String levelUp() {
        level++;
        return name + " levelled up, they are now level " + level;
    }

    // ---------
    // GENERATED
    // ---------

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public PrimaryAttribute getCharacterAttributes() {
        return characterAttributes;
    }
}
