package no.accelerate.hero.action;

import no.accelerate.hero.Rogue;
import no.accelerate.hero.Warrior;
import no.accelerate.item.Armor;
import no.accelerate.item.Weapon;
import no.accelerate.item.exception.InvalidArmorException;
import no.accelerate.item.exception.InvalidWeaponException;
import no.accelerate.item.util.ArmorType;
import no.accelerate.item.util.WeaponType;
import no.accelerate.util.equipment.Slot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class WarriorEquippingTest {
    private Warrior hero;

    @BeforeEach
    public void setup() {
        hero = new Warrior("TestWarrior");
    }

    @Test
    public void equip_validWeapon_shouldReturnProperMessage() {
        // Arrange
        Weapon testWeapon = new Weapon("TestWeapon",1,
                WeaponType.AXE,1,1);
        String expected = "Weapon equipped!";
        // Act
        String actual = assertDoesNotThrow(() -> hero.equip(testWeapon));
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void equip_invalidWeaponType_shouldThrowInvalidWeaponExceptionWithProperMessage() {
        // Arrange
        Weapon testWeapon = new Weapon("TestWeapon",1,
                WeaponType.BOW,1,1);
        String expected = "Wrong weapon type!";
        // Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> hero.equip(testWeapon));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void equip_invalidWeaponLevel_shouldThrowInvalidWeaponExceptionWithProperMessage() {
        // Arrange
        Weapon testWeapon = new Weapon("TestWeapon",2,
                WeaponType.AXE,1,1);
        String expected = "Weapon level too high!";
        // Act
        Exception exception = assertThrows(InvalidWeaponException.class, () -> hero.equip(testWeapon));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void equip_validArmor_shouldReturnProperMessage() {
        // Arrange
        Armor testArmor = new Armor("TestArmor", Slot.BODY,
                1, ArmorType.PLATE,1,1,1);
        String expected = "Armor equipped!";
        // Act
        String actual = assertDoesNotThrow(() -> hero.equip(testArmor));
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void equip_invalidArmorType_shouldReturnProperMessage() {
        // Arrange
        Armor testArmor = new Armor("TestArmor", Slot.BODY,
                1, ArmorType.CLOTH,1,1,1);
        String expected = "Wrong armor type!";
        // Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> hero.equip(testArmor));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void equip_invalidArmorLevel_shouldReturnProperMessage() {
        // Arrange
        Armor testArmor = new Armor("TestArmor", Slot.BODY,
                2, ArmorType.PLATE,1,1,1);
        String expected = "Armor level too high!";
        // Act
        Exception exception = assertThrows(InvalidArmorException.class, () -> hero.equip(testArmor));
        String actual = exception.getMessage();
        // Assert
        assertEquals(expected,actual);
    }
}
