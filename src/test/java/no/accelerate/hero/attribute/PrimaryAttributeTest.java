package no.accelerate.hero.attribute;

import no.accelerate.util.attribute.PrimaryAttribute;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class to verify behaviour of the PrimaryAttributes class.
 */
public class PrimaryAttributeTest {
    // New instances made before each test with setup()
    private PrimaryAttribute primaryAttribute;

    @BeforeEach
    public void setup() {
        primaryAttribute = new PrimaryAttribute(1,2,3);
    }

    @Test
    public void create_default_shouldHaveCorrectStrength() {
        // Arrange
        int expected = 1;
        // Act
        int actual = primaryAttribute.getStrength();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void create_default_shouldHaveCorrectDexterity() {
        // Arrange
        int expected = 2;
        // Act
        int actual = primaryAttribute.getDexterity();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void create_default_shouldHaveCorrectIntelligence() {
        // Arrange
        int expected = 3;
        // Act
        int actual = primaryAttribute.getIntelligence();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void increase_default_shouldIncreaseByCorrectAmount() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(1+1,2+2, 3+3);
        // Act
        primaryAttribute.increase(1,2,3);
        PrimaryAttribute actual = primaryAttribute; // Bit awkward, could be refactored
        // Assert
        assertEquals(expected,actual);
    }
}
