package no.accelerate.hero.attribute;

import no.accelerate.hero.Warrior;
import no.accelerate.util.attribute.PrimaryAttribute;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for verifying warrior attribute behaviour.
 */
public class WarriorAttributeTest {
    // New instances made before each test with setupWarrior()
    private Warrior warrior;

    @BeforeEach
    public void setupWarrior() {
        warrior = new Warrior("TestWarrior");
    }

    @Test
    public void create_defaultRogue_shouldHaveCorrectName() {
        // Arrange
        String expected = "TestWarrior";
        // Act
        String actual = warrior.getName();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void create_defaultWarrior_shouldHaveCorrectStartingAttributes() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(5,2,1);
        // Act
        PrimaryAttribute actual = warrior.getCharacterAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelup_defaultWarrior_shouldIncreaseAttributesByCorrectAmount() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(5 + 3,2 + 2,1 + 1);
        // Act
        warrior.levelUp();
        PrimaryAttribute actual = warrior.getCharacterAttributes();
        // Assert
        assertEquals(expected, actual);
    }
}
