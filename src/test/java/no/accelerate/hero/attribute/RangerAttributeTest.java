package no.accelerate.hero.attribute;

import no.accelerate.hero.Ranger;
import no.accelerate.util.attribute.PrimaryAttribute;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for verifying ranger attribute behaviour.
 */
public class RangerAttributeTest {
    // New instances made before each test with setupRanger()
    private Ranger ranger;

    @BeforeEach
    public void setupRanger() {
        ranger = new Ranger("TestRanger");
    }

    @Test
    public void create_defaultRanger_shouldHaveCorrectName() {
        // Arrange
        String expected = "TestRanger";
        // Act
        String actual = ranger.getName();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void create_defaultRanger_shouldHaveCorrectStartingAttributes() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(1,7,1);
        // Act
        PrimaryAttribute actual = ranger.getCharacterAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelup_defaultRanger_shouldIncreaseAttributesByCorrectAmount() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(1 + 1,7 + 5,1 + 1);
        // Act
        ranger.levelUp();
        PrimaryAttribute actual = ranger.getCharacterAttributes();
        // Assert
        assertEquals(expected, actual);
    }
}
