package no.accelerate.hero.attribute;

import no.accelerate.hero.Mage;
import no.accelerate.util.attribute.PrimaryAttribute;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for verifying mage attribute behaviour.
 */
public class MageAttributeTest {
    // New instances made before each test with setupMage()
    private Mage mage;

    @BeforeEach
    public void setupMage() {
        mage = new Mage("TestMage");
    }

    @Test
    public void create_defaultMage_shouldHaveCorrectName() {
        // Arrange
        String expected = "TestMage";
        // Act
        String actual = mage.getName();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void create_defaultMage_shouldHaveCorrectStartingAttributes() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(1,1,8);
        // Act
        PrimaryAttribute actual = mage.getCharacterAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelup_defaultMage_shouldIncreaseAttributesByCorrectAmount() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(1 + 1,1 + 1,8 + 5);
        // Act
        mage.levelUp();
        PrimaryAttribute actual = mage.getCharacterAttributes();
        // Assert
        assertEquals(expected, actual);
    }
}
