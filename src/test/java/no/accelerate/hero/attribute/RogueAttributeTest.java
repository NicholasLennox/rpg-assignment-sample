package no.accelerate.hero.attribute;

import no.accelerate.hero.Rogue;
import no.accelerate.util.attribute.PrimaryAttribute;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for verifying rogue attribute behaviour.
 */
public class RogueAttributeTest {
    // New instances made before each test with setupRogue()
    private Rogue rogue;

    @BeforeEach
    public void setupRogue() {
        rogue = new Rogue("TestRogue");
    }

    @Test
    public void create_defaultRogue_shouldHaveCorrectName() {
        // Arrange
        String expected = "TestRogue";
        // Act
        String actual = rogue.getName();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void create_defaultRogue_shouldHaveCorrectStartingAttributes() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(2,6,1);
        // Act
        PrimaryAttribute actual = rogue.getCharacterAttributes();
        // Assert
        assertEquals(expected, actual);
    }

    @Test
    public void levelup_defaultRogue_shouldIncreaseAttributesByCorrectAmount() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(2 + 1,6 + 4,1 + 1);
        // Act
        rogue.levelUp();
        PrimaryAttribute actual = rogue.getCharacterAttributes();
        // Assert
        assertEquals(expected, actual);
    }
}
