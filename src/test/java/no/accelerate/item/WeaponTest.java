package no.accelerate.item;

import no.accelerate.item.util.WeaponType;
import no.accelerate.util.equipment.Slot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for verifying weapon setup behaviour.
 */
public class WeaponTest {
    // New instances made before each test with setupWeapon()
    private Weapon weapon;

    @BeforeEach
    public void setupWeapon() {
        weapon = new Weapon("TestWeapon", 1, WeaponType.AXE, 1.1, 1.2);
    }

    @Test
    public void create_simpleWeapon_shouldHaveCorrectName() {
        // Arrange
        String expected = "TestWeapon";
        // Act
        String actual = weapon.getName();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void create_simpleWeapon_shouldHaveCorrectSlot() {
        // Arrange
        Slot expected = Slot.WEAPON;
        // Act
        Slot actual = weapon.getSlot();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void create_simpleWeapon_shouldHaveCorrectRequiredLevel() {
        // Arrange
        int expected = 1;
        // Act
        int actual = weapon.getRequiredLevel();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void create_simpleWeapon_shouldHaveCorrectWeaponType() {
        // Arrange
        WeaponType expected = WeaponType.AXE;
        // Act
        WeaponType actual = weapon.getWeaponType();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void create_simpleWeapon_shouldHaveCorrectDps() {
        // Arrange
        double expected = 1.1 * 1.2;
        // Act
        double actual = weapon.getDps();
        // Assert
        assertEquals(expected,actual);
    }
}
