package no.accelerate.item;

import no.accelerate.item.util.ArmorType;
import no.accelerate.util.attribute.PrimaryAttribute;
import no.accelerate.util.equipment.Slot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for verifying armor setup behaviour.
 */
public class ArmorTest {
    // New instances made before each test with setupArmor()
    private Armor armor;

    @BeforeEach
    public void setupArmor() {
        armor = new Armor("TestArmor", Slot.BODY, 1, ArmorType.CLOTH, 1,2,3);
    }

    @Test
    public void create_simpleArmor_shouldHaveCorrectName() {
        // Arrange
        String expected = "TestArmor";
        // Act
        String actual = armor.getName();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void create_simpleArmor_shouldHaveCorrectSlot() {
        // Arrange
        Slot expected = Slot.BODY;
        // Act
        Slot actual = armor.getSlot();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void create_simpleArmor_shouldHaveCorrectRequiredLevel() {
        // Arrange
        int expected = 1;
        // Act
        int actual = armor.getRequiredLevel();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void create_simpleArmor_shouldHaveArmorType() {
        // Arrange
        ArmorType expected = ArmorType.CLOTH;
        // Act
        ArmorType actual = armor.getArmorType();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void create_simpleArmor_shouldHaveCorrectBonusAttributes() {
        // Arrange
        PrimaryAttribute expected = new PrimaryAttribute(1,2,3);
        // Act
        PrimaryAttribute actual = armor.getBonusAttributes();
        // Assert
        assertEquals(expected,actual);
    }
}
